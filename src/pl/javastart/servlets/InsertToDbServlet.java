package pl.javastart.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jdbcConnection.MySQLConnector;

import TableModels.ConfInfo;
import TableModels.ConferenceInfo;
import TableModels.Form;

@WebServlet(urlPatterns = "/insert")
public class InsertToDbServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private final int maxForms = 5;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		MySQLConnector database = new MySQLConnector(maxForms);

		Form form = buildFormFromRequest(request);

		// do testow
		printForm(form, response, request);

		if (!formRegisteredInSession(request)) {

			if (emailsAreMatching(request)) {
				if (database.formExistsInDataBase(form)) {
					setResponseFormExists(response);
				} else {
					if (database.numberOfFormsRegistered() < maxForms) {
						database.insertForm(form);
						request.getSession().setAttribute("User_Inserted",
								"true");
						setResponseFormInserted(response);
					} else {
						setResponseTooManyForms(response);
					}
				}
			} else {
				setResponseEmailsAreNotMatching(response);
			}
		} else {
			setResponseYouAlreadyRegistered(response);
		}

	}

	private boolean formRegisteredInSession(HttpServletRequest request) {
		if (request.getSession().getAttribute("User_Inserted") != null
				&& request.getSession().getAttribute("User_Inserted") == "true")
			return true;
		return false;
	}

	private void setResponseYouAlreadyRegistered(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			// out.print("<meta http-equiv='refresh' ;url='sessionRegistered'>");
			out.print("Twoje zgloszenie jest juz zarejstrowane <br/>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void setResponseEmailsAreNotMatching(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Niestety Podane Emaile sie nie zgadzaja. <br/>");
			out.print("<a href='form'>Sproboj ponownie</a>");
			// out.print("<meta http-equiv='refresh' ;url='incorectEmails'>");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private boolean emailsAreMatching(HttpServletRequest request) {
		// Email_Confirm

		if (request.getParameter("Email").equals( request
				.getParameter("Email_Confirm"))
				&& request.getParameter("Email") != null) {
			return true;
		}
		return false;
	}

	private Form buildFormFromRequest(HttpServletRequest request) {
		Form form = new Form();
		form.setFirstName(request.getParameter("First_Name"));
		form.setLastName(request.getParameter("Last_Name"));
		form.setEmail(request.getParameter("Email"));
		form.setEmployer(request.getParameter("Employer"));
		ConferenceInfo confInfo = new ConferenceInfo();
		confInfo.setOption(optionFromRequest(request));
		confInfo.setOthersWhich(request.getParameter("Others_Which"));
		form.setConferenceInfo(confInfo);
		form.setHobbies(request.getParameter("Hobbies"));
		return form;
	}

	private ConfInfo optionFromRequest(HttpServletRequest request) {
		String option = request.getParameter("Option");
		if (option == ConfInfo.facebook.toString()) {
			return ConfInfo.facebook;
		} else if (option == ConfInfo.friends.toString()) {
			return ConfInfo.friends;
		} else if (option == ConfInfo.job_ad.toString()) {
			return ConfInfo.job_ad;
		} else if (option == ConfInfo.university_ad.toString()) {
			return ConfInfo.university_ad;
		} else {
			return ConfInfo.other;
		}
	}

	private void setResponseFormInserted(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Formularz przyjety");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setResponseFormExists(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Ta osoba juz sie zarejestrowala");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void setResponseTooManyForms(HttpServletResponse response) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("Niestety mamy juz za duzo formularzy");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void printForm(Form form, HttpServletResponse response, HttpServletRequest request) {
		PrintWriter out;
		try {
			response.setContentType("text/html");
			out = response.getWriter();
			out.print("<br/>Imie : " + form.getFirstName());
			out.print("<br/>Nazwisko : " + form.getLastName());
			out.print("<br/>Email : " + form.getEmail());
			out.print("<br/>Pracodawca : " + form.getEmployer());
			out.print("<br/>Skad sie dowiedzial : "
					+ form.getConferenceInfo().getOption().toString());
			out.print("<br/> Opis : "
					+ form.getConferenceInfo().getOthersWhich());
			out.print("<br/>Czym sie zajmuje : " + form.getHobbies());
			out.print("<br/>Email potwierdzajacy : " + request.getParameter("Email_Confirm"));
			out.print("<br/>");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
package pl.javastart.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(urlPatterns = "/form")
public class FormServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");

		PrintWriter out = response.getWriter();
		out.println("<html><body><h2>Simple form servlet</h2>" + "<form action='insert'>"
				+ "First name: <input type='text' name='First_Name' /> <br />"
				+ "Last name: <input type='text' name='Last_Name' /> <br />"
				+ "Email: <input type='text' name='Email' /> <br />"
				+ "Confirm Email: <input type='text' name='Email_Confirm' /> <br />"
				+ "Employer: <input type='text' name='Employer' /> <br />" 
				
				+ "How did you find out about 'Java 4 US!' ? <br />"
				+ "<input type='radio' name='group1' value='job_ad' /> Job Advertisement <br />"
				+ "<input type='radio' name='group1' value='university_ad' /> University Advertisement <br />"
				+ "<input type='radio' name='group1' value='facebook' /> Facebook <br />"
				+ "<input type='radio' name='group1' value='friends' /> Friends <br />"
				+ "<input type='radio' name='group1' value='other' /> Others : Which ? <br />"
					
			    + "<textarea cols='40' rows='5' name='Others_Which'>"
				+ "Please Describe."
				+ "</textarea>"
			
				+ "<br/> Please tell us about your hobbies : <br/>"
				+ "<textarea cols='40' rows='5' name='Hobbies'>"
				+ "...."
				+ "</textarea>"
				
				+ "<input type='submit' value=' OK ' />" + "</form>" + "</body></html>");
		out.close();

	}
}
